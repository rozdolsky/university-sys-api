%dw 2.0
output application/json
---
payload map (p) -> {
	id: p.id,
	name: p.name,
	courses: vars.courses filter ($.university_id == p.id) map (cr) ->
	(cr.name) : {
		id: cr.id,
		name: cr.instructor_name
	}

}
