%dw 2.0
output application/json
---
payload map ((course , index) -> {
	id: course.id,
	name: course.name,
	instructorName: course.instructor_name
})