%dw 2.0
output application/json
var university = vars.university[0]
---
	{
	id: university.id,
	name: university.name,
	courses: payload map (cr)-> {
		(cr.name) : {
			id: cr.id,
			name: cr.instructor_name
		} 
	  }
	}