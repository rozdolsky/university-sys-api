%dw 2.0
output application/java
---
{
	name: payload.name as String, 
	instructor_name: payload.instructor_name as String,
	university_id: payload.university_id as Number
}