%dw 2.0
output application/java
---
 {
	Id: attributes.uriParams.id as Number,
	name: payload.name,
	instructor_name: payload.instructor_name,
	university_id: payload.university_id as Number
}
